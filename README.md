# BAP - Technical Test

## Install
```javascript
npm install
```

## start
```javascript
npm start
```


## Config
You can change and play with the configuration file in `/src/config/index.js`
All other files should not be changed

## Explanations
- Pink badges indicates the number of equipement
- Grey badges indicates the equipement consumption in `l/min`


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
