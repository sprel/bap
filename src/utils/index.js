import { AMENITIES, FLATS } from '../config';

export const getFlatConsumption = ({flat, extensions}) => {
	// We get consumption of flat preset equipments
	const arrayBaseConsumption = Object.entries(FLATS[flat]).map(([type, number]) => number*AMENITIES[type].consumption);
	// And we check if this flat has some extensions
	const arrayExtendConsumption = extensions ? Object.entries(extensions).map(([type, number]) => number*AMENITIES[type].consumption) : [];
	// SO we got arrays of cosumptions, we sum it and return the full flat consumption
	return [...arrayBaseConsumption, ...arrayExtendConsumption].reduce((p, c) => p+c, 0);
}

export const getFloorConsumption = floor => {
	const arrayFloorConsumption = floor.map(flat => getFlatConsumption(flat));
	return [...arrayFloorConsumption].reduce((p, c) => p+c, 0);
}

export const getBuildingConsumption = floors => {
	const arrayBuildingConsumption = floors.map(floor => getFloorConsumption(floor));
	return [...arrayBuildingConsumption].reduce((p, c) => p+c, 0);
}

export const getBlockConsumption = buildings => {
	const arrayClockConsumption = buildings.map(building => getBuildingConsumption(building.floors));
	return [...arrayClockConsumption].reduce((p, c) => p+c, 0);
}

export const getFlatAmenities = ({flat, extensions}) => {
	// Getting flat equipment details: number, type, icon, and consumption.
	// We could improve this function to group equipement. for exemple, if we have 2 bathtub by flat preset, and one more bathtub with extension, this will result in displaying the bathtub icon twice.
	const arrayBaseAmenities = Object.entries(FLATS[flat]).map(([type, number]) => ({
		id: type,
		number,
		...AMENITIES[type]
	}));
	const arrayExtendAmenities = extensions ? Object.entries(extensions).map(([type, number]) => ({
		id: type,
		number,
		...AMENITIES[type]
	})) : [];
	return [...arrayBaseAmenities, ...arrayExtendAmenities];
}
