// You can add as many equipments
export const AMENITIES = {
	sink: {
		icon: 'sink.svg',
		consumption: 2,
	},
	bathtub: {
		icon: 'bathtub.svg',
		consumption: 5,
	},
	shower: {
		icon: 'shower.svg',
		consumption: 1
	},
	jacuzzi: {
		icon: 'jacuzzi.svg',
		consumption: 10
	}
}

// Flat presets
export const FLATS = {
	big: {
		sink: 2,
		bathtub: 1,
	},
	small: {
		shower: 1,
		sink: 1,
	}
}

// add/remove building and configure floors and flats by floors
export const BUILDINGS = {
	a: {
		floors: [
			[{flat: 'small'}, {flat: 'small'}, {flat: 'small'}, {flat: 'small'}],
			[{flat: 'small'}, {flat: 'small'}, {flat: 'small'}, {flat: 'small'}],
		]
	},
	b: {
		floors: [
			[{flat: 'small'}, {flat: 'small'}, {flat: 'small'}, {flat: 'small'}],
			[{flat: 'small'}, {flat: 'small'}, {flat: 'small'}, {flat: 'small'}],
			[{flat: 'small'}, {flat: 'small'}, {flat: 'small'}, {flat: 'small'}],
		]
	},
	c: {
		floors: [
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
		]
	},
	d: {
		floors: [
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big', extensions: { jacuzzi: 1}}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
			[{flat: 'big'}, {flat: 'big'}],
		]
	}
}

// Refresh rate
export const REFRESH_RATE = 50;
