import React from 'react';
import format from 'simple-number-formatter';

export default ({count, elapsed, label}) => {
	return (
		<div className="counter">
			{label}{format(Math.floor(count*(elapsed/1000/60)*100)/100, '0.00')}
		</div>
	);
}
