import React, { Component } from 'react';
import '../css/index.css';

import Building from './Building';
import Counter from './Counter';
import { getBlockConsumption } from '../utils';

import { BUILDINGS, REFRESH_RATE } from '../config';

class App extends Component {

	state = { elapsed: 0 };
	totalConsumption = 0;

	componentDidMount() {
		this.startTime = new Date().getTime();
		this.tick = setInterval(this.handleTick, REFRESH_RATE);
		this.totalConsumption = getBlockConsumption(Object.values(BUILDINGS));
	}

	handleTick = () => {
		// We use time comparison instead of timeout tick, to prevent laggy effects when scrolling
		const elapsed = (new Date().getTime())-this.startTime;
		this.setState({elapsed})
	}

	conponentWillUnmount() {
		clearInterval(this.tick);
	}

	render() {
		const { elapsed } = this.state;
		return (
			<div className="app">
				<div className="buildings">
				{ Object.entries(BUILDINGS).map(([id, building]) => <Building
																		key={id}
																		id={id}
																		datas={building}
																		elapsed={elapsed}
																	/>
												)
				}
				</div>
				<Counter label="Total consumption: " count={this.totalConsumption} elapsed={elapsed} />
			</div>
		)
	}
}

export default App;
