import React, { Component } from 'react';

import Counter from './Counter';
import { getFlatConsumption, getFlatAmenities } from '../utils';


export default class Flat extends Component {

	totalConsumption = 0;
	amenities = [];

	componentDidMount() {
		const { datas } = this.props;
		this.totalConsumption = getFlatConsumption(datas);
		this.amenities = getFlatAmenities(datas);
	}

	render() {
		const {id, building, floor, elapsed} = this.props;
		return (
			<div className="flat">
				{building.toUpperCase()}-{floor === 0 ? "RDC" : floor}-{(id+1)}
				<div className="icos">
					{this.amenities.map(({id, icon, number, consumption}) => (
							<div className="ico">
								<img src={`${process.env.PUBLIC_URL}/icos/${icon}`} alt={id} />
								{number > 1 && <div className="badge"><span>{number}</span></div>}
								<div className="badge badge--br"><span>{consumption}</span></div>
							</div>
						)
					)}
				</div>
				<Counter count={this.totalConsumption} elapsed={elapsed} />
			</div>
		);
	}
}
