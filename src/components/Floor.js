import React, { Component } from 'react';

import Flat from './Flat';
import Counter from './Counter';
import { getFloorConsumption } from '../utils';

export default class Floor extends Component {

	totalConsumption = 0;

	componentDidMount() {
		this.totalConsumption = getFloorConsumption(this.props.datas);
	}

	render() {
		const  { id, building, datas, elapsed } = this.props;

		return (
			<div className="floor">
				<div className="flats">
				{ datas.map((flat, i) => 	<Flat
												key={i}
												building={building}
												floor={id}
												id={i}
												datas={flat}
												elapsed={elapsed}
												onTotalConsumption={this.onTotalConsumption}
											/>
							)
				}
				</div>
				<Counter label="Floor: " count={this.totalConsumption} elapsed={elapsed} />
			</div>
		);
	}
}
