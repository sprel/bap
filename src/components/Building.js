import React, { Component } from 'react';

import Floor from './Floor';
import Counter from './Counter';
import { getBuildingConsumption } from '../utils';

export default class Building extends Component {

	totalConsumption = 0;

	componentDidMount() {
		this.totalConsumption = getBuildingConsumption(this.props.datas.floors);
	}

	render() {
		const { id, datas, elapsed } = this.props;
		return (
			<div className="building">
				<div className="floors">
				{ datas.floors.map((floor, i) => (
					<Floor
						building={id}
						id={i}
						key={i}
						datas={floor}
						elapsed={elapsed}
					/>
				))}
				</div>
				<Counter label="Building: " count={this.totalConsumption} elapsed={elapsed} />
			</div>
		);
	}
}
